//
//  SavedArticlesTableViewController.m
//  tnytnews
//
//  Created by Usuario Curso de IOS on 22/08/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "SavedArticlesTableViewController.h"
#import "WebViewController.h"
#import "AppDelegate.h"
#import "CustomTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SavedArticlesTableViewController ()

@property (strong, nonatomic) NSMutableArray * articles;

@end

@implementation SavedArticlesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated {
    
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    self.articles = delegate.articles;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.articles) {
        return self.articles.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSavedArticles" forIndexPath:indexPath];
    
    cell.labelTitleCell.text = [self.articles[indexPath.row] objectForKey:@"title"];
    cell.labelDescriptionCell.text = [self.articles[indexPath.row] objectForKey:@"abstract"];
    
    if (![[self.articles[indexPath.row] objectForKey:@"multimedia"] isKindOfClass:[NSString class ]]) {
        NSString *imageURL = [[[self.articles[indexPath.row] objectForKey:@"multimedia"] objectAtIndex:0] objectForKey:@"url"];
        [cell.imageViewCell sd_setImageWithURL:[NSURL URLWithString:imageURL]
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    } else {
        [cell.imageViewCell setImage:[UIImage imageNamed:@"default.png"]];
    }
    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        AppDelegate * delegate = [UIApplication sharedApplication].delegate;
        if (![delegate delete:[self.articles objectAtIndex:indexPath.row]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível deletar Article." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView reloadData];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"showArticle"]) {
        WebViewController * controller = segue.destinationViewController;
        NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
        CustomTableViewCell *cell = (CustomTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        controller.article = self.articles[indexPath.row];
        controller.title = cell.labelTitleCell.text;
    }
}

@end
