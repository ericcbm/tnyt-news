//
//  TopStoriesTableViewController.m
//  tnytnews
//
//  Created by Usuario Curso de IOS on 22/08/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "PoliticsTableViewController.h"
#import "WebViewController.h"
#import <AFNetworking.h>
#import "CustomTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PoliticsTableViewController ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSArray *articles;

@end

@implementation PoliticsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Puxe para Atualizar"];
    [refresh addTarget:self action:@selector(loadNews)
     
    forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated {
    self.articles = [NSArray array];
    [self.tableView reloadData];
    
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    [self loadNews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.articles) {
        return self.articles.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellPolitics" forIndexPath:indexPath];
    
    cell.labelTitleCell.text = [self.articles[indexPath.row] objectForKey:@"title"];
    cell.labelDescriptionCell.text = [self.articles[indexPath.row] objectForKey:@"abstract"];
    
    if (![[self.articles[indexPath.row] objectForKey:@"multimedia"] isKindOfClass:[NSString class ]]) {
        NSString *imageURL = [[[self.articles[indexPath.row] objectForKey:@"multimedia"] objectAtIndex:0] objectForKey:@"url"];
        [cell.imageViewCell sd_setImageWithURL:[NSURL URLWithString:imageURL]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    } else {
        [cell.imageViewCell setImage:[UIImage imageNamed:@"default.png"]];
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"showArticle"]) {
        WebViewController * controller = segue.destinationViewController;
        NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
        CustomTableViewCell *cell = (CustomTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        controller.article = self.articles[indexPath.row];
        controller.title = cell.labelTitleCell.text;
    }
}

#pragma mark - Internal Methods

- (void)loadNews {
    //http://api.nytimes.com/svc/topstories/v1/home.json?api-key=52485bf4bffa27f672d99191c03e1e27:9:72746260
    AFHTTPRequestOperationManager *afManager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"52485bf4bffa27f672d99191c03e1e27:9:72746260", @"api-key",
                            nil];
    [afManager GET:@"http://api.nytimes.com/svc/topstories/v1/politics.json" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"%@", responseObject);
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"ok");
            self.articles = [NSArray arrayWithArray:[responseObject objectForKey:@"results"]];
            [self.tableView reloadData];
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
        } else {
            NSLog(@"Error");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar Politics." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar Politics." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        NSLog(@"Error: %@", error);
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];
    }];
    [self performSelector:@selector(stopRefresh)];
}

- (void)stopRefresh
{
    [self.refreshControl endRefreshing];
}

@end
