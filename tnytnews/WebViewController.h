//
//  WebViewController.h
//  tnytnews
//
//  Created by Usuario Curso de IOS on 22/08/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate> {
}

@property (weak, nonatomic) NSMutableDictionary * article;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
- (IBAction)onSaveClick:(id)sender;

@end
