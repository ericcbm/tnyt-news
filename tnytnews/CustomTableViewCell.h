//
//  CustomTableViewCell.h
//  tnytnews
//
//  Created by Usuario Curso de IOS on 29/08/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitleCell;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCell;
@property (weak, nonatomic) IBOutlet UILabel *labelDescriptionCell;

@end
